#! /usr/bin/env bash
set -euo pipefail

###############################################################################
# Help/usage messages #########################################################
###############################################################################
SCRIPT_NAME="$(basename "${0}")"
USAGE="Usage:
  ${SCRIPT_NAME} [OPTIONS] [TEST_REGEX] [EXTRA_OPTIONS]
  ${SCRIPT_NAME} [-h | --help]"

HELP="${SCRIPT_NAME}
  This script provides a single interface for running Go tests with 'go test'
  or debugging Go tests with go-delve, using 'dlv test'.

  TEST_REGEX is an optional positional parameter. If provided, only tests
  matching it will be run.

  EXTRA_OPTIONS are any flags (with optional values) that can be used by
  'go test' or 'dlv test'. They should not match top-level flags used by
  this script.

${USAGE}

Options:
  -b, --benchmark           Run benchmarks instead of tests.
  -c, --coverage=FILE       Write a coverage profile to the provided FILE. Not
                            compatible with debug mode.
  -d, --debug               Debug test(s) with go-delve. If this flag is not
                            provided, the tests will be run by 'go test'.
  -l, --clean               If provided, the Go test cache will be cleaned
                            before running any tests.
  -n, --count               Number of times to repeat the tests. Not compatible
                            with debug mode. [default: 1]
  -p, --path                Path to search for tests to run. [default: ./]
                            directory) for tests to run.
  -r, --race                Add race detection. Not compatible with debug mode.
  -t, --test-tags=TAGS      Comma-separated list of Go build tags to use for
                            matching tests. [default: unit]
                            the 'clean' command.
  -q, --quiet               Turn off verbose output.
  -h, --help                Print this message and exit.

Examples:
  ${SCRIPT_NAME}                      # Run all tests.
  ${SCRIPT_NAME} -d '^TestList$'      # Debug the 'TestList' test.
  ${SCRIPT_NAME} -t integration       # Run tests with the 'integration' tag."


###############################################################################
# Helpers #####################################################################
###############################################################################
declare -r SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )
. "${SCRIPT_DIR}/.helpers/all"

function clean() {
    # Usage: clean
    go clean -testcache
}

function debug_tests() {
    # Usage: debug_tests <test-path> <test-tag> [test-regex]
    #local CMD="dlv test ${1} --build-flags=\"-tags=${2}\" -- -test.v"
    local CMD="dlv test ${1} --build-flags=\"-tags=${2}\""

    if [[ "${BENCHMARK:-0}" -eq 1 ]]; then
        local BENCHMARK_REGEX="${3:-./}"
        EXTRA_OPTS+="-test.bench='${BENCHMARK_REGEX}' -test.benchmem -test.run='^$'"
    elif [[ -n "${3}" ]]; then
        EXTRA_OPTS+="-test.run '${3}'"
    fi

    if [[ -n "${EXTRA_OPTS}" ]]; then
        CMD="${CMD} -- ${EXTRA_OPTS}"
    fi

    echo "${CMD}"
    eval "${CMD}"
}

function run_tests() {
    # Usage: run_tests <test-path> <test-tag> [test-regex]
    # Information on race checking and coverage calculations are passed in
    # through global environment variables instead of through args.

    # Check if race checking is requested
    local CMD="go test --tags=\"${2}\" -count ${COUNT}${EXTRA_OPTS} ${1}"
    if [[ "${RACE:-0}" -eq 1 ]]; then
        CMD+=" -race"
    fi

    # Check if coverage file is requested
    if [[ -n "${COVERAGE_FILE}" ]]; then
        CMD+=" -coverprofile=${COVERAGE_FILE}"
    fi

    # Check verbosity
    if [[ "${VERBOSE}" -eq 1 ]]; then
        CMD+=" -v"
    fi

    # Check if a test name regex is provided
    if [[ "${BENCHMARK:-0}" -eq 1 ]]; then
        local BENCHMARK_REGEX="${3:-./}"
        CMD+=" -bench='${BENCHMARK_REGEX}' -benchmem -test.run='^$'"
    elif [[ -n "${3}" ]]; then
        CMD+=" -test.run '${3}'"
    fi

    echo "${CMD}"
    eval "${CMD}"
}

###############################################################################
# Main code ###################################################################
###############################################################################
# Make sure Go is installed
if ! command -v go > /dev/null; then
    echo "This script requires the Go compiler, but it does not appear to be installed."
    exit 1
fi
# Make sure dlv is installed
if ! command -v dlv > /dev/null; then
    echo "This script requires go-delve, but it does not appear to be installed."
    exit 1
fi

# Parse flags
VERBOSE=1
CLEAN=0
DEBUG=0
BENCHMARK=0
COVERAGE_FILE=""
RACE=0
COUNT=1
TEST_PATH=./
TEST_REGEX=""
TEST_TAGS="unit"
EXTRA_OPTS=""
POSITIONAL_ARGS=()
while [[ $# -gt 0 ]]; do
    case $1 in
        -b|--benchmark)
            BENCHMARK=1
            shift
            ;;

        -c|--coverage|--coverage=*)
            COVERAGE_FILE=$(parse_flag "${1}" "${2:-}")
            [[ "${1}" == *"="* ]] || shift
            shift
            ;;

        -d|--debug)
            DEBUG=1
            shift
            ;;

        -l|--clean)
            CLEAN=1
            shift
            ;;

        -n|--count|--count=*)
            COUNT=$(parse_flag "${1}" "${2:-}")
            [[ "${1}" == *"="* ]] || shift
            shift
            ;;

        -p|--path|--path=*)
            TEST_PATH=$(parse_flag "${1}" "${2:-}")
            [[ "${1}" == *"="* ]] || shift
            shift
            ;;

        -q|--quiet)
            VERBOSE=0
            shift
            ;;

        -r|--race)
            RACE=1
            shift
            ;;

        -t|--test-tags|--test-tags=*)
            TEST_TAGS=$(parse_flag "${1}" "${2:-}")
            [[ "${1}" == *"="* ]] || shift
            shift
            ;;

        -h|--help)
            echo "${HELP}"
            exit 0
            ;;

        -*|--*)
            EXTRA_OPTS+=" ${1}"
            if [[ -n "${2:-}" ]] && [[ "${2:-}" != "-"* ]]; then
                EXTRA_OPTS+=" ${2}"
                shift
            fi
            shift
            ;;

        *)
            POSITIONAL_ARGS+=("${1}") # save positional arg
            shift
            ;;

    esac
done

# Reset positional args and check length
set -- "${POSITIONAL_ARGS[@]}"
if [[ "$#" -eq 1 ]]; then
    TEST_REGEX="${1}"
elif [[ "$#" -gt 1 ]]; then
    echo "Only one positional argument is allowed."
    echo "${USAGE}"
    exit 1
fi

# Clean test cache, if requested
if [[ "${CLEAN}" -eq 1 ]]; then
    clean
fi

# Run or debug tests
if [[ "${DEBUG}" -eq 0 ]]; then
    run_tests "${TEST_PATH}" "${TEST_TAGS}" "${TEST_REGEX}"
else
    # Check if specifying multiple packages with $TEST_PATH (not allowed)
    if [[ "${TEST_PATH}" == *"..."* ]]; then
        echo "Cannot specify multiple packages when debugging tests."
        exit 1
    fi

    debug_tests "${TEST_PATH}" "${TEST_TAGS}" "${TEST_REGEX}"
fi
