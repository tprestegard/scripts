#! /usr/bin/env bash

#########
# Setup #
#########
set -euo pipefail

#########################
# Validate/process args #
#########################
if [[ "$#" -ne 2 ]]; then
    echo "Requires two arguments."
    echo "Usage: $(basename "${0}") <ORIGINAL-EXPR> <REPLACEMENT-STR>"
    exit 1
fi
ORIGINAL_STR="${1}"
NEW_STR="${2}"

#############
# Main code #
#############
# If there is a pipe character in either one, we need to escape it for use with sed.
# But we don't escape it for use with grep.
ESCAPED_ORIGINAL_STR="${ORIGINAL_STR//|/\\|}"
NEW_STR="${2//|/\\|}"

# Run grep/sed
grep -ERl "${ORIGINAL_STR}" | xargs sed -E -i "s|${ESCAPED_ORIGINAL_STR}|${NEW_STR}|g"
